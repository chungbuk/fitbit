package kr.co.cbnu.fitbit;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.test.context.ActiveProfiles;

@SpringBootApplication
@EnableScheduling
@ActiveProfiles("production")
public class FitbitApplication {

	public static void main(String[] args) {
		SpringApplication.run(FitbitApplication.class, args);
	}
}