package kr.co.cbnu.fitbit.scheduling;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;

import kr.co.cbnu.fitbit.entities.ActivitiesHeart;
import kr.co.cbnu.fitbit.entities.ActivitiesHeartIntraday;
import kr.co.cbnu.fitbit.entities.Dataset;
import kr.co.cbnu.fitbit.entities.FitbitUser;
import kr.co.cbnu.fitbit.entities.HeartRateZone;
import kr.co.cbnu.fitbit.services.ActivitiesHeartIntradayService;
import kr.co.cbnu.fitbit.services.ActivitiesHeartService;
import kr.co.cbnu.fitbit.services.FitbitUserService;

@Component
public class ActivitiesHeartJob {

	@Autowired
	private ActivitiesHeartService activitiesHeartService;
	
	@Autowired
	private ActivitiesHeartIntradayService activitiesHeartIntradayService;
	
	@Autowired
	private FitbitUserService fitbitUserService;
	
	//TODO: TO DO EVERYDAY AT 12:00
	//@Scheduled(cron="0 0 11 * * *")
	public void heartActivities() throws JsonParseException, JsonMappingException, IOException{
		
		System.out.println(new Date().toString());
		
		RestTemplate restTemplate = new RestTemplate();
		
		List<FitbitUser> fitbitUsers = fitbitUserService.findAll();
		
		for(FitbitUser fitbitUser : fitbitUsers){
			
			System.out.println(fitbitUser);
			HttpHeaders headers = new HttpHeaders();
			headers.add("Authorization", "Bearer "+ fitbitUser.getAccessToken());
			
			HttpEntity<Object> requestBody = new HttpEntity<>(headers);
			Map<String, Object> response = (Map<String, Object>) restTemplate.exchange("https://api.fitbit.com/1/user/{userId}/activities/heart/date/today/1d.json", HttpMethod.GET, requestBody, Map.class, fitbitUser.getEncodedId()).getBody();
			
			List<Map> responseActivitiesHearts = (List<Map>) response.get("activities-heart");
			
			List<ActivitiesHeart> activitiesHearts = new ArrayList<>();
			
			for(Map map: responseActivitiesHearts){
				ActivitiesHeart activitiesHeart = new ActivitiesHeart();
				activitiesHeart.setUserId(fitbitUser.getEncodedId());
				activitiesHeart.setDateTime((String) map.get("dateTime"));
				activitiesHearts.add(activitiesHeart);
				
				List<Map> responseHeartRateZones = (List<Map>) ((Map)map.get("value")).get("heartRateZones");
				
				for(Map responseHeartRateZone : responseHeartRateZones){
					HeartRateZone heartRateZone = new HeartRateZone();
					heartRateZone.setCaloriesOut((responseHeartRateZone.get("caloriesOut")==null)? "0" : responseHeartRateZone.get("caloriesOut") + "");
					heartRateZone.setMax((int) responseHeartRateZone.get("max"));
					heartRateZone.setMin((int) responseHeartRateZone.get("min"));
					heartRateZone.setMinutes((int) ((responseHeartRateZone.get("minutes")==null) ? 0 : responseHeartRateZone.get("minutes")));
					heartRateZone.setName((String) responseHeartRateZone.get("name"));
					activitiesHeart.getHeartRateZones().add(heartRateZone);
				}
				
				activitiesHearts.add(activitiesHeart);
				activitiesHeartService.saveActivitiesHeart(activitiesHeart);
			}
			
			System.out.println(activitiesHearts);
		}

	}
	
	//TODO: TO DO EVERYDAY
	@Scheduled(cron="0 0 0 * * *")
	public void activitiesHeartIntraday() throws JsonParseException, JsonMappingException, IOException{
		
		System.out.println(new Date().toString());
		
		RestTemplate restTemplate = new RestTemplate();
		
		List<FitbitUser> fitbitUsers = fitbitUserService.findAll();
		
//		for(int i=-6 ;i<=0; i++){
		
		String datetime = "";
		
		SimpleDateFormat dateFormat = new SimpleDateFormat("HH:MM");
		Calendar calendar = Calendar.getInstance();
		String endTime = dateFormat.format(calendar.getTime()).toString();
		calendar.add(Calendar.HOUR, -1);
		String startTime = dateFormat.format(calendar.getTime()).toString();
		
		SimpleDateFormat dateFormatDate = new SimpleDateFormat("YYYY-MM-dd");
		
		Calendar todayCalendar = Calendar.getInstance();
		
		todayCalendar.add(Calendar.DATE, -1);
		
		String today = dateFormatDate.format(todayCalendar.getTime());
		
		datetime = today;
		
		//today = "2016-09-10";
		System.out.println("DATE ==> " + today +", START TIME==>" + startTime +", END TIME==>" + endTime);
		
		startTime ="00:00";
		endTime = "23:59";
		
		for(FitbitUser fitbitUser : fitbitUsers){
			
			System.out.println(fitbitUser);
			HttpHeaders headers = new HttpHeaders();
			headers.add("Authorization", "Bearer "+ fitbitUser.getAccessToken());
			
			HttpEntity<Object> requestBody = new HttpEntity<>(headers);
			//https://api.fitbit.com/1/user/-/activities/heart/date/2016-09-10/1d/1sec/time/00:00/00:01.json
			Map<String, Object> response = (Map<String, Object>) restTemplate.exchange("https://api.fitbit.com/1/user/{userId}/activities/heart/date/{today}/1d/1sec/time/{startTime}/{endTime}.json", HttpMethod.GET, requestBody, Map.class, fitbitUser.getEncodedId(), datetime, startTime, endTime).getBody();
			
			System.out.println("RESPONSE==>" + response);
			
			List<Map> responseActivitiesHearts = (List<Map>) response.get("activities-heart");
			
			List<ActivitiesHeart> activitiesHearts = new ArrayList<>();
							
			for(Map map: responseActivitiesHearts){
				ActivitiesHeart activitiesHeart = new ActivitiesHeart();
				activitiesHeart.setUserId(fitbitUser.getEncodedId());
				activitiesHeart.setDateTime(map.get("dateTime") + "");
				activitiesHearts.add(activitiesHeart);
				
				List<Map> responseHeartRateZones = (List<Map>)map.get("heartRateZones");
				
				for(Map responseHeartRateZone : responseHeartRateZones){
					HeartRateZone heartRateZone = new HeartRateZone();
					heartRateZone.setCaloriesOut((responseHeartRateZone.get("caloriesOut")==null)? "0" : responseHeartRateZone.get("caloriesOut") + "");
					heartRateZone.setMax((int) responseHeartRateZone.get("max"));
					heartRateZone.setMin((int) responseHeartRateZone.get("min"));
					heartRateZone.setMinutes((int) ((responseHeartRateZone.get("minutes")==null) ? 0 : responseHeartRateZone.get("minutes")));
					heartRateZone.setName((String) responseHeartRateZone.get("name"));
					activitiesHeart.getHeartRateZones().add(heartRateZone);
				}
				
				activitiesHearts.add(activitiesHeart);
				activitiesHeartService.saveActivitiesHeart(activitiesHeart);
			}
			
			Map responseActivitiesHeartsIntraday = (Map) response.get("activities-heart-intraday");
			
			List<ActivitiesHeartIntraday> activitiesHeartIntradays = new ArrayList<>();
			
			ActivitiesHeartIntraday activitiesHeartIntraday = new ActivitiesHeartIntraday();
			activitiesHeartIntraday.setDatetime(datetime);
			activitiesHeartIntraday.setDatasetInterval(responseActivitiesHeartsIntraday.get("datasetInterval") + "");
			activitiesHeartIntraday.setDatasetType(responseActivitiesHeartsIntraday.get("datasetType") + "");
			activitiesHeartIntraday.setUserId(fitbitUser.getEncodedId());
			
			List<Map> responseDatasets = (List<Map>) (responseActivitiesHeartsIntraday.get("dataset"));
			
			for(Map responseDataset : responseDatasets){
				Dataset dataset = new Dataset();
				dataset.setTime(responseDataset.get("time") + "");
				dataset.setValue((int)responseDataset.get("value"));
				activitiesHeartIntraday.getDatasets().add(dataset);
			}
			activitiesHeartIntradays.add(activitiesHeartIntraday);
			activitiesHeartIntradayService.saveActivitiesHeartIntraday(activitiesHeartIntraday);
		}
//		}
	}
		
//	public static void main(String args[]) {
//		SimpleDateFormat dateFormat = new SimpleDateFormat("HH:MM");
//		Calendar calendar = Calendar.getInstance();
//		String currentTime = dateFormat.format(calendar.getTime()).toString();
//		calendar.add(Calendar.HOUR, -1);
//		String oneHourBack = dateFormat.format(calendar.getTime()).toString();
//			
//	}

}
