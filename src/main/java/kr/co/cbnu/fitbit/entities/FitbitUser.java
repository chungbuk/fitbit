package kr.co.cbnu.fitbit.entities;

public class FitbitUser {

	private String encodedId; 
	private int age;
	private String avatar;
	private String avatar150;
	private int averageDailySteps;
	private String city;
	private String clockTimeDisplayFormat;
	private String corperate;
	private String corperateAdmin;
	private String country;
	private String dateOfBirth;
	private String displayName;
	private String distanceUnit;
	private String foodsLocale;
	private String fullName;
	private String glucoseUnit;
	private String locale;
	private String memberSince;
	private String offsetFromUTCMillis;
	private String startDayOfWeek;
	private String strideLengthRunning;
	private String strideLengthRunningType;
	private String strideLengthWalking;
	private String strideLengthWalkingType;
	private String timezone;
	private String waterUnit;
	private String waterUnitName;
	private double weight;
	private String weightUnit;
	private String accessToken;
	
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age = age;
	}
	public String getAvatar() {
		return avatar;
	}
	public void setAvatar(String avatar) {
		this.avatar = avatar;
	}
	public String getAvatar150() {
		return avatar150;
	}
	public void setAvatar150(String avatar150) {
		this.avatar150 = avatar150;
	}
	public int getAverageDailySteps() {
		return averageDailySteps;
	}
	public void setAverageDailySteps(int averageDailySteps) {
		this.averageDailySteps = averageDailySteps;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getClockTimeDisplayFormat() {
		return clockTimeDisplayFormat;
	}
	public void setClockTimeDisplayFormat(String clockTimeDisplayFormat) {
		this.clockTimeDisplayFormat = clockTimeDisplayFormat;
	}
	public String getCorperate() {
		return corperate;
	}
	public void setCorperate(String corperate) {
		this.corperate = corperate;
	}
	public String getCorperateAdmin() {
		return corperateAdmin;
	}
	public void setCorperateAdmin(String corperateAdmin) {
		this.corperateAdmin = corperateAdmin;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public String getDateOfBirth() {
		return dateOfBirth;
	}
	public void setDateOfBirth(String dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}
	public String getDisplayName() {
		return displayName;
	}
	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}
	public String getDistanceUnit() {
		return distanceUnit;
	}
	public void setDistanceUnit(String distanceUnit) {
		this.distanceUnit = distanceUnit;
	}
	public String getEncodedId() {
		return encodedId;
	}
	public void setEncodedId(String encodedId) {
		this.encodedId = encodedId;
	}
	public String getFoodsLocale() {
		return foodsLocale;
	}
	public void setFoodsLocale(String foodsLocale) {
		this.foodsLocale = foodsLocale;
	}
	public String getFullName() {
		return fullName;
	}
	public void setFullName(String fullName) {
		this.fullName = fullName;
	}
	public String getGlucoseUnit() {
		return glucoseUnit;
	}
	public void setGlucoseUnit(String glucoseUnit) {
		this.glucoseUnit = glucoseUnit;
	}
	public String getLocale() {
		return locale;
	}
	public void setLocale(String locale) {
		this.locale = locale;
	}
	public String getMemberSince() {
		return memberSince;
	}
	public void setMemberSince(String memberSince) {
		this.memberSince = memberSince;
	}
	public String getOffsetFromUTCMillis() {
		return offsetFromUTCMillis;
	}
	public void setOffsetFromUTCMillis(String offsetFromUTCMillis) {
		this.offsetFromUTCMillis = offsetFromUTCMillis;
	}
	public String getStartDayOfWeek() {
		return startDayOfWeek;
	}
	public void setStartDayOfWeek(String startDayOfWeek) {
		this.startDayOfWeek = startDayOfWeek;
	}
	public String getStrideLengthRunning() {
		return strideLengthRunning;
	}
	public void setStrideLengthRunning(String strideLengthRunning) {
		this.strideLengthRunning = strideLengthRunning;
	}
	public String getStrideLengthRunningType() {
		return strideLengthRunningType;
	}
	public void setStrideLengthRunningType(String strideLengthRunningType) {
		this.strideLengthRunningType = strideLengthRunningType;
	}
	public String getStrideLengthWalking() {
		return strideLengthWalking;
	}
	public void setStrideLengthWalking(String strideLengthWalking) {
		this.strideLengthWalking = strideLengthWalking;
	}
	public String getStrideLengthWalkingType() {
		return strideLengthWalkingType;
	}
	public void setStrideLengthWalkingType(String strideLengthWalkingType) {
		this.strideLengthWalkingType = strideLengthWalkingType;
	}
	public String getTimezone() {
		return timezone;
	}
	public void setTimezone(String timezone) {
		this.timezone = timezone;
	}
	public String getWaterUnit() {
		return waterUnit;
	}
	public void setWaterUnit(String waterUnit) {
		this.waterUnit = waterUnit;
	}
	public String getWaterUnitName() {
		return waterUnitName;
	}
	public void setWaterUnitName(String waterUnitName) {
		this.waterUnitName = waterUnitName;
	}
	public double getWeight() {
		return weight;
	}
	public void setWeight(double weight) {
		this.weight = weight;
	}
	public String getWeightUnit() {
		return weightUnit;
	}
	public void setWeightUnit(String weightUnit) {
		this.weightUnit = weightUnit;
	}
	public String getAccessToken() {
		return accessToken;
	}
	public void setAccessToken(String accessToken) {
		this.accessToken = accessToken;
	}
	@Override
	public String toString() {
		return "FitbitUser [encodedId=" + encodedId + ", age=" + age + ", avatar=" + avatar + ", avatar150=" + avatar150
				+ ", averageDailySteps=" + averageDailySteps + ", city=" + city + ", clockTimeDisplayFormat="
				+ clockTimeDisplayFormat + ", corperate=" + corperate + ", corperateAdmin=" + corperateAdmin
				+ ", country=" + country + ", dateOfBirth=" + dateOfBirth + ", displayName=" + displayName
				+ ", distanceUnit=" + distanceUnit + ", foodsLocale=" + foodsLocale + ", fullName=" + fullName
				+ ", glucoseUnit=" + glucoseUnit + ", locale=" + locale + ", memberSince=" + memberSince
				+ ", offsetFromUTCMillis=" + offsetFromUTCMillis + ", startDayOfWeek=" + startDayOfWeek
				+ ", strideLengthRunning=" + strideLengthRunning + ", strideLengthRunningType="
				+ strideLengthRunningType + ", strideLengthWalking=" + strideLengthWalking
				+ ", strideLengthWalkingType=" + strideLengthWalkingType + ", timezone=" + timezone + ", waterUnit="
				+ waterUnit + ", waterUnitName=" + waterUnitName + ", weight=" + weight + ", weightUnit=" + weightUnit
				+ ", accessToken=" + accessToken + "]";
	}
	
}
