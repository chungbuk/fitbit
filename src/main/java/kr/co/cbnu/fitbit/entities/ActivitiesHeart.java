package kr.co.cbnu.fitbit.entities;

import java.util.ArrayList;
import java.util.List;

public class ActivitiesHeart {

	private int id;
	private String dateTime;
	private String userId;
	private List<HeartRateZone> heartRateZones = new ArrayList<>();
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getDateTime() {
		return dateTime;
	}
	public void setDateTime(String dateTime) {
		this.dateTime = dateTime;
	}
	public List<HeartRateZone> getHeartRateZones() {
		return heartRateZones;
	}
	public void setHeartRateZones(List<HeartRateZone> heartRateZones) {
		this.heartRateZones = heartRateZones;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	@Override
	public String toString() {
		return "ActivitiesHeart [id=" + id + ", dateTime=" + dateTime + ", heartRateZones=" + heartRateZones + "]";
	}
	
}
