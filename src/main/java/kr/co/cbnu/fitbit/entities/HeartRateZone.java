package kr.co.cbnu.fitbit.entities;

public class HeartRateZone {

	private String caloriesOut;
	private int max;
	private int min;
	private int minutes;
	private String name;
	
	public String getCaloriesOut() {
		return caloriesOut;
	}
	public void setCaloriesOut(String caloriesOut) {
		this.caloriesOut = caloriesOut;
	}
	public int getMax() {
		return max;
	}
	public void setMax(int max) {
		this.max = max;
	}
	public int getMin() {
		return min;
	}
	public void setMin(int min) {
		this.min = min;
	}
	public int getMinutes() {
		return minutes;
	}
	public void setMinutes(int minutes) {
		this.minutes = minutes;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	@Override
	public String toString() {
		return "HeartRateZone [caloriesOut=" + caloriesOut + ", max=" + max + ", min=" + min + ", minutes=" + minutes
				+ ", name=" + name + ", getCaloriesOut()=" + getCaloriesOut() + ", getMax()=" + getMax() + ", getMin()="
				+ getMin() + ", getMinutes()=" + getMinutes() + ", getName()=" + getName() + ", getClass()="
				+ getClass() + ", hashCode()=" + hashCode() + ", toString()=" + super.toString() + "]";
	}
	
}
