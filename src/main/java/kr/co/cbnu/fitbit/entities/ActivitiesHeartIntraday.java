package kr.co.cbnu.fitbit.entities;

import java.util.ArrayList;
import java.util.List;

public class ActivitiesHeartIntraday {
	
	private int id;
	private String datasetInterval;
	private String datasetType;
	private List<Dataset> datasets = new ArrayList<>();
	private String userId;
	private String datetime;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getDatasetInterval() {
		return datasetInterval;
	}
	public void setDatasetInterval(String datasetInterval) {
		this.datasetInterval = datasetInterval;
	}
	public String getDatasetType() {
		return datasetType;
	}
	public void setDatasetType(String datasetType) {
		this.datasetType = datasetType;
	}
	public List<Dataset> getDatasets() {
		return datasets;
	}
	public void setDatasets(List<Dataset> datasets) {
		this.datasets = datasets;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getDatetime() {
		return datetime;
	}
	public void setDatetime(String datetime) {
		this.datetime = datetime;
	}
	@Override
	public String toString() {
		return "ActivitiesHeartIntraday [id=" + id + ", datasetInterval=" + datasetInterval + ", datasetType="
				+ datasetType + ", datasets=" + datasets + ", userId=" + userId + ", datetime=" + datetime + "]";
	}
	
}
