package kr.co.cbnu.fitbit.entities;

import com.fasterxml.jackson.annotation.JsonProperty;

public class FitbitAuthentication {
	@JsonProperty("ACCESS_TOKEN")
	private String accessToken;
	@JsonProperty("USER_ID")
	private String userId;
	
	public String getAccessToken() {
		return accessToken;
	}
	public void setAccessToken(String accessToken) {
		this.accessToken = accessToken;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	
}
