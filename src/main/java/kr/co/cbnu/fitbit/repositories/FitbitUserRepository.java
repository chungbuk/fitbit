package kr.co.cbnu.fitbit.repositories;

import java.util.List;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;
import org.springframework.stereotype.Repository;

import kr.co.cbnu.fitbit.entities.FitbitUser;

@Repository
public interface FitbitUserRepository {

	@Insert("INSERT INTO fitbit_users(encoded_id"
			+ "						, age"
			+ "						, avatar"
			+ "						, avatar150"
			+ "						, average_daily_steps"
			+ "						, display_name"
			+ "						, city"
			+ "						, corperate"
			+ "						, clock_time_display_format"
			+ "						, corperate_admin"
			+ "						, country"
			+ "						, date_of_birth"
			+ "						, distance_unit"
			+ "						, foods_locale"
			+ "						, glucose_unit"
			+ "						, locale"
			+ "						, member_since"
			+ "						, offset_from_utc_millis"
			+ "						, start_day_of_week"
			+ "						, stride_length_running"
			+ "						, stride_length_running_type"
			+ "						, stride_length_walking"
			+ "						, stride_length_walking_type"
			+ "						, timezone"
			+ "						, water_unit"
			+ "						, water_unit_name"
			+ "						, weight"
			+ "						, weight_unit "
			+ "						, access_token) "
			+ "VALUES(#{encodedId}"
			+ "	  , #{age}"
			+ "	  , #{avatar}"
			+ "	  , #{avatar150}"
			+ "	  , #{averageDailySteps}"
			+ "	  , #{displayName}"
			+ "	  , #{city}"
			+ "	  , #{corperate}"
			+ "	  , #{clockTimeDisplayFormat}"
			+ "	  , #{corperateAdmin}"
			+ "	  , #{country}"
			+ "	  , #{dateOfBirth}"
			+ "   , #{distanceUnit}"
			+ "   , #{foodsLocale}"
			+ "   , #{glucoseUnit}"
			+ "	  , #{locale}"
			+ "   , #{memberSince}"
			+ "   , #{offsetFromUTCMillis}"
			+ "   , #{startDayOfWeek}"
			+ "	  , #{strideLengthRunning}"
			+ "	  , #{strideLengthRunningType}"
			+ "	  , #{strideLengthWalking}"
			+ "	  , #{strideLengthWalkingType}"
			+ "	  , #{timezone}"
			+ "	  , #{waterUnit}"
			+ "	  , #{waterUnitName}"
			+ "	  , #{weight}"
			+ "	  , #{weightUnit}"
			+ "	  , #{accessToken})")
	public int save(FitbitUser fitbitUser);
	
	@Select("SELECT encoded_id "
		  + "	  ,	age "
		  + "	  , avatar "
		  + "	  , avatar150 "
		  + "	  , average_daily_steps "
		  + "     , display_name "
		  + "     , city "
		  + "     , corperate "
		  + "     , clock_time_display_format "
		  + "     , corperate_admin "
		  + "     , country "
		  + "     , date_of_birth "
		  + "     , distance_unit "
		  + "     , foods_locale "
		  + "     , glucose_unit "
		  + "     , locale "
		  + "     , member_since "
		  + "     , offset_from_utc_millis "
		  + "     , start_day_of_week "
		  + "     , stride_length_running "
		  + "     , stride_length_running_type "
		  + "     , stride_length_walking "
		  + "     , stride_length_walking_type "
		  + "     , timezone "
		  + "     , water_unit "
		  + "     , water_unit_name "
		  + "     , weight "
		  + "     , weight_unit "
		  + "	  , access_token "
		  + "FROM fitbit_users "
		  + "WHERE encoded_id = #{id}")
	@Results({
		@Result(property="encodedId", column="encoded_id"),
		@Result(property="age", column="age"),
		@Result(property="avatar", column="avatar"),
		@Result(property="avatar150", column="avatar150"),
		@Result(property="averageDailySteps", column="average_daily_steps"),
		@Result(property="city", column="city"),
		@Result(property="corperate", column="corperate"),
		@Result(property="clockTimeDisplayFormat", column="clock_time_display_format"),
		@Result(property="corperateAdmin", column="corperate_admin"),
		@Result(property="country", column="country"),
		@Result(property="dateOfBirth", column="date_of_birth"),
		@Result(property="distanceUnit", column="distance_unit"),
		@Result(property="foodsLocale", column="foods_locale"),
		@Result(property="glucoseUnit", column="glucose_unit"),
		@Result(property="locale", column="locale"),
		@Result(property="memberSince", column="member_since"),
		@Result(property="offsetFromUTCMillis", column="offset_from_utc_millis"),
		@Result(property="startDayOfWeek", column="start_day_of_week"),
		@Result(property="strideLengthRunning", column="stride_length_running"),
		@Result(property="strideLengthRunningType", column="stride_length_running_type"),
		@Result(property="strideLengthWalking", column="stride_length_walking"),
		@Result(property="strideLengthWalkingType", column="stride_length_walking_type"),
		@Result(property="timezone", column="timezone"),
		@Result(property="waterUnit", column="water_unit"),
		@Result(property="waterUnitName", column="water_unit_name"),
		@Result(property="weight", column="weight"),
		@Result(property="weightUnit", column="weight_unit"),
		@Result(property="accessToken", column="access_token")
	})
	public FitbitUser findOne(String id);
	
	@Select("SELECT encoded_id "
			  + "	  ,	age "
			  + "	  , avatar "
			  + "	  , avatar150 "
			  + "	  , average_daily_steps "
			  + "     , display_name "
			  + "     , city "
			  + "     , corperate "
			  + "     , clock_time_display_format "
			  + "     , corperate_admin "
			  + "     , country "
			  + "     , date_of_birth "
			  + "     , distance_unit "
			  + "     , foods_locale "
			  + "     , glucose_unit "
			  + "     , locale "
			  + "     , member_since "
			  + "     , offset_from_utc_millis "
			  + "     , start_day_of_week "
			  + "     , stride_length_running "
			  + "     , stride_length_running_type "
			  + "     , stride_length_walking "
			  + "     , stride_length_walking_type "
			  + "     , timezone "
			  + "     , water_unit "
			  + "     , water_unit_name "
			  + "     , weight "
			  + "     , weight_unit "
			  + "	  , access_token "
			  + "FROM fitbit_users ")
	@Results({
		@Result(property="encodedId", column="encoded_id"),
		@Result(property="age", column="age"),
		@Result(property="avatar", column="avatar"),
		@Result(property="avatar150", column="avatar150"),
		@Result(property="averageDailySteps", column="average_daily_steps"),
		@Result(property="city", column="city"),
		@Result(property="corperate", column="corperate"),
		@Result(property="clockTimeDisplayFormat", column="clock_time_display_format"),
		@Result(property="corperateAdmin", column="corperate_admin"),
		@Result(property="country", column="country"),
		@Result(property="dateOfBirth", column="date_of_birth"),
		@Result(property="distanceUnit", column="distance_unit"),
		@Result(property="foodsLocale", column="foods_locale"),
		@Result(property="glucoseUnit", column="glucose_unit"),
		@Result(property="locale", column="locale"),
		@Result(property="memberSince", column="member_since"),
		@Result(property="offsetFromUTCMillis", column="offset_from_utc_millis"),
		@Result(property="startDayOfWeek", column="start_day_of_week"),
		@Result(property="strideLengthRunning", column="stride_length_running"),
		@Result(property="strideLengthRunningType", column="stride_length_running_type"),
		@Result(property="strideLengthWalking", column="stride_length_walking"),
		@Result(property="strideLengthWalkingType", column="stride_length_walking_type"),
		@Result(property="timezone", column="timezone"),
		@Result(property="waterUnit", column="water_unit"),
		@Result(property="waterUnitName", column="water_unit_name"),
		@Result(property="weight", column="weight"),
		@Result(property="weightUnit", column="weight_unit"),
		@Result(property="accessToken", column="access_token")
	})
	
	public List<FitbitUser> findAll();
	
	@Update("UPDATE fitbit_users SET access_token = #{accessToken} WHERE encoded_id = #{encodedId}")
	public int updateAccessToken(FitbitUser fitbitUser);
	
	
}