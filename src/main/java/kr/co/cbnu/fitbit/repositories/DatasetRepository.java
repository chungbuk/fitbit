package kr.co.cbnu.fitbit.repositories;

import org.apache.ibatis.annotations.Insert;
import org.springframework.stereotype.Repository;

import kr.co.cbnu.fitbit.entities.ActivitiesHeartIntraday;

@Repository
public interface DatasetRepository {

	@Insert(  "<script> "
			+ "INSERT INTO dataset(activities_heart_intraday_id"
			+ "							  , time"
			+ "							  , value) "
			+ "VALUES "
			+ "		<foreach collection='datasets' item='dataset' separator=',' >"
			+ "			(#{id}"
			+ "		   , #{dataset.time}"
			+ "		   , #{dataset.value})"
			+ "		</foreach> "
			+ "</script>")
	public void save(ActivitiesHeartIntraday activitiesHeartIntraday);
}
