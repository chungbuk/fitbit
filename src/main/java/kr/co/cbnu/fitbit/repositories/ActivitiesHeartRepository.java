package kr.co.cbnu.fitbit.repositories;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.SelectKey;
import org.springframework.stereotype.Repository;

import kr.co.cbnu.fitbit.entities.ActivitiesHeart;

@Repository
public interface ActivitiesHeartRepository {

	@Insert("INSERT INTO activities_hearts(activities_hearts_id, datetime, encoded_id) "
		  + "VALUES(#{id}, #{dateTime}, #{userId})")
	@SelectKey(before=true, keyProperty="id", statement="SELECT NEXT VALUE FOR activities_hearts_activities_hearts_id_seq", resultType = int.class)
	public int save(ActivitiesHeart activitiesHeart);
	
	@Select("SELECT activities_hearts_id, encoded_id, datetime FROM activities_hearts WHERE datetime = #{dateTime}")
	@Results({
		@Result(property="id", column="activities_hearts_id"),
		@Result(property="userId", column="encoded_id"),
		@Result(property="dateTime", column="datetime")
	})
	public ActivitiesHeart findOneByDateTime(String dateTime);
}
