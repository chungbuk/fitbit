package kr.co.cbnu.fitbit.repositories;

import org.apache.ibatis.annotations.Insert;
import org.springframework.stereotype.Repository;

import kr.co.cbnu.fitbit.entities.ActivitiesHeart;

@Repository
public interface HeartRateZoneRepository{
	
	@Insert(  "<script> "
			+ "INSERT INTO heart_rate_zones(activities_heart_id"
			+ "							  , calories_out"
			+ "							  , max"
			+ "							  , min"
			+ "						      , minutes"
			+ "							  , name) "
			+ "VALUES "
			+ "		<foreach collection='heartRateZones' item='heartRateZone' separator=',' >"
			+ "			(#{id}"
			+ "		   , #{heartRateZone.caloriesOut}"
			+ "		   , #{heartRateZone.max}"
			+ "		   , #{heartRateZone.min}"
			+ "		   , #{heartRateZone.minutes}"
			+ "		   , #{heartRateZone.name})"
			+ "		</foreach> "
			+ "</script>")
	public void save(ActivitiesHeart activitiesHeart);
}
