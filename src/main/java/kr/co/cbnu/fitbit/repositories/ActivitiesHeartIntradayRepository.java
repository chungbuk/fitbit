package kr.co.cbnu.fitbit.repositories;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.SelectKey;
import org.springframework.stereotype.Repository;

import kr.co.cbnu.fitbit.entities.ActivitiesHeartIntraday;

@Repository
public interface ActivitiesHeartIntradayRepository {

	@Insert("INSERT INTO activities_heart_intraday(activities_heart_intraday_id, dataset_interval, dataset_type, encoded_id, datetime) "
		  + "VALUES(#{id}, #{datasetInterval}, #{datasetType}, #{userId}, #{datetime})")
	@SelectKey(before=true, keyProperty="id", statement="SELECT NEXT VALUE FOR activities_heart_intraday_activities_hearts_intraday_id_seq", resultType = int.class)
	public int save(ActivitiesHeartIntraday activitiesHeartIntraday);
	
	@Select("SELECT activities_heart_intraday_id, dataset_interval,encoded_id, dataset_type, encoded_id, datetime "
		  + "FROM activities_heart_intraday "
		  + "WHERE dataset_interval = #{datasetInterval} "
		  + "AND dataset_type = #{datasetType} "
		  + "AND datetime = #{dateTime} "
		  + "AND encoded_id = #{userId}")
	@Results({
		@Result(property="id", column="activities_heart_intraday_id"),
		@Result(property="userId", column="encoded_id"),
		@Result(property="datetime", column="datetime"),
		@Result(property="datasetType", column="dataset_type"),
		@Result(property="datasetInterval", column="dataset_interval")
	})
	public ActivitiesHeartIntraday findOneByDateTimeAndUserIdAndDatasetTypeAndDatasetInterval(@Param("dateTime") String dateTime
																					, @Param("userId") String userId
																					, @Param("datasetType") String datasetType
																					, @Param("datasetInterval") String datasetInterval);
	
}
