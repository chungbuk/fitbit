package kr.co.cbnu.fitbit.services.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import kr.co.cbnu.fitbit.entities.ActivitiesHeart;
import kr.co.cbnu.fitbit.repositories.ActivitiesHeartRepository;
import kr.co.cbnu.fitbit.repositories.HeartRateZoneRepository;
import kr.co.cbnu.fitbit.services.ActivitiesHeartService;

@Service
public class ActivitiesHeartServiceImpl implements ActivitiesHeartService{

	@Autowired
	private ActivitiesHeartRepository activitiesHeartRepository;
	
	@Autowired
	private HeartRateZoneRepository heartRateZoneRepository;
	
	@Override
	@Transactional
	public void saveActivitiesHeart(ActivitiesHeart activitiesHeart) {
		try{
			if(activitiesHeartRepository.findOneByDateTime(activitiesHeart.getDateTime())==null){
				activitiesHeartRepository.save(activitiesHeart);
				heartRateZoneRepository.save(activitiesHeart);
			}
		}catch(Exception ex){
			ex.printStackTrace();
		}
		
	}

}
