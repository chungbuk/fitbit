package kr.co.cbnu.fitbit.services;

import java.util.List;

import kr.co.cbnu.fitbit.entities.FitbitUser;

//TODO: TO CONTROL ON FitbitUser
public interface FitbitUserService {

	//TODO: TO SAVE FitbitUser
	public void save(FitbitUser fitbitUser);
	
	//TODO: TO FIND FitbitUser By Id
	public FitbitUser findOne(String id);
	
	//TODO: TO FIND ALL FitbitUser 
	public List<FitbitUser> findAll();
	
}
