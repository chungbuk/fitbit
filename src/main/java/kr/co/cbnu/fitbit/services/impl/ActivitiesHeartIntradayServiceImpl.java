package kr.co.cbnu.fitbit.services.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import kr.co.cbnu.fitbit.entities.ActivitiesHeartIntraday;
import kr.co.cbnu.fitbit.entities.Dataset;
import kr.co.cbnu.fitbit.repositories.ActivitiesHeartIntradayRepository;
import kr.co.cbnu.fitbit.repositories.DatasetRepository;
import kr.co.cbnu.fitbit.services.ActivitiesHeartIntradayService;

@Service
public class ActivitiesHeartIntradayServiceImpl implements ActivitiesHeartIntradayService {

	@Autowired
	private DatasetRepository datasetRepository;
	
	@Autowired
	private ActivitiesHeartIntradayRepository activitiesHeartIntradayRepository;
	
	@Override
	@Transactional
	public void saveActivitiesHeartIntraday(ActivitiesHeartIntraday activitiesHeartIntraday) {
		if(activitiesHeartIntradayRepository.findOneByDateTimeAndUserIdAndDatasetTypeAndDatasetInterval(activitiesHeartIntraday.getDatetime(), activitiesHeartIntraday.getUserId(), activitiesHeartIntraday.getDatasetType(), activitiesHeartIntraday.getDatasetInterval()) == null){
			activitiesHeartIntradayRepository.save(activitiesHeartIntraday);
			//if(activitiesHeartIntraday.getDatasets().size()>0){
			System.out.println("SIZE ==>" + activitiesHeartIntraday.getDatasets().size());
			List<Dataset> list = activitiesHeartIntraday.getDatasets();
			if(list.size()>0){
				int total = list.size();        
		        int interval = 100;
		        int from = 0;
		        int to = 0;
		        while (to <= total)
		        {
		            from = to == 0 ? 0 : to;
		            to = (to + interval) <= total ? (to + interval) : total;
		            activitiesHeartIntraday.setDatasets(list.subList(from, to));
		            datasetRepository.save(activitiesHeartIntraday);
		            if (to == total){
		            	System.out.println("TO ==> " + to + " TOTAL ==> " + total);
		            	break;		            	
		            }
		        }			
			}
		}
		
	}

}
