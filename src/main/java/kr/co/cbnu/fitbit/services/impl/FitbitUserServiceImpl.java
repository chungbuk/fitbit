package kr.co.cbnu.fitbit.services.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import kr.co.cbnu.fitbit.entities.FitbitUser;
import kr.co.cbnu.fitbit.repositories.FitbitUserRepository;
import kr.co.cbnu.fitbit.services.FitbitUserService;

@Service
public class FitbitUserServiceImpl implements FitbitUserService{

	@Autowired
	private FitbitUserRepository fitbitUserRepository;
	
	@Override
	public void save(FitbitUser fitbitUser) {
		try{
			FitbitUser user = fitbitUserRepository.findOne(fitbitUser.getEncodedId());
			if(user==null){
				fitbitUserRepository.save(fitbitUser);
			}else{
				fitbitUserRepository.updateAccessToken(fitbitUser);
			}
		}catch(Exception ex){
			ex.printStackTrace();
		}
	}

	@Override
	public FitbitUser findOne(String id) {
		try{
			return fitbitUserRepository.findOne(id);
		}catch(Exception ex){
			System.out.println(ex.getMessage());
			ex.printStackTrace();
		}
		return null;
	}

	@Override
	public List<FitbitUser> findAll() {
		try{
			return fitbitUserRepository.findAll();
		}catch(Exception ex){
			ex.printStackTrace();
		}
		return null;
	}
	
	
}
