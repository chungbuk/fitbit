package kr.co.cbnu.fitbit.services;

import kr.co.cbnu.fitbit.entities.ActivitiesHeart;

public interface ActivitiesHeartService {
	
	public void saveActivitiesHeart(ActivitiesHeart activitiesHeart);
	
}
