package kr.co.cbnu.fitbit.services;

import kr.co.cbnu.fitbit.entities.ActivitiesHeartIntraday;

public interface ActivitiesHeartIntradayService {

	public void saveActivitiesHeartIntraday(ActivitiesHeartIntraday activitiesHeartIntraday);
}
