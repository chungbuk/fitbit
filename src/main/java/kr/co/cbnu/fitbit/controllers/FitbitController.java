package kr.co.cbnu.fitbit.controllers;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.client.RestTemplate;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import kr.co.cbnu.fitbit.entities.FitbitAuthentication;
import kr.co.cbnu.fitbit.entities.FitbitUser;
import kr.co.cbnu.fitbit.services.FitbitUserService;
import springfox.documentation.annotations.ApiIgnore;

@Controller
@RequestMapping(value="/v1/api/fitbit")
@Api(value="Heart Rate")
public class FitbitController {
	
	private static String ACCESS_TOKEN;
	
	@Autowired
	private FitbitUserService fitbitUserService;

	@RequestMapping(value="/access-token", method=RequestMethod.GET)
	@ApiIgnore
	public String callback(@RequestParam(value="access_token", required=false) String accessToken, @RequestParam(value="user_id", required=false) String userId ){
		System.out.println("ACCESS TOKEN==>" + accessToken);
		System.out.println("USER ID==>" + userId);
		FitbitAuthentication fitbitAuthentication = new FitbitAuthentication();
		fitbitAuthentication.setAccessToken(accessToken);
		fitbitAuthentication.setUserId(userId);
		FitbitController.ACCESS_TOKEN = accessToken;
		
		RestTemplate restTemplate = new RestTemplate();
		
		HttpHeaders headers = new HttpHeaders();
		headers.add("Authorization", "Bearer "+ACCESS_TOKEN);
		
		HttpEntity<Object> requestBody = new HttpEntity<>(headers);
		@SuppressWarnings("unchecked")
		Map<String, Object> response = (Map<String, Object>) restTemplate.exchange("https://api.fitbit.com/1/user/{userId}/profile.json", HttpMethod.GET, requestBody, Map.class, userId).getBody().get("user");
		
		System.out.println("RESPONSE==> " + response);
		FitbitUser fitbitUser = new FitbitUser();
		fitbitUser.setEncodedId(response.get("encodedId") + "");
		fitbitUser.setDisplayName(response.get("displayName") + "");
		fitbitUser.setAge((int)response.get("age"));
		fitbitUser.setAvatar(response.get("avatar") + "");
		fitbitUser.setAvatar150(response.get("avatar150") + "");
		fitbitUser.setAverageDailySteps((int)response.get("averageDailySteps"));
		fitbitUser.setCity(response.get("city") + "");
		fitbitUser.setCorperate(response.get("corperate") + "");
		fitbitUser.setCountry(response.get("country") + "");
		fitbitUser.setClockTimeDisplayFormat(response.get("clockTimeDisplayFormat") + "");
		fitbitUser.setCorperateAdmin(response.get("corperateAdmin") + "");
		fitbitUser.setDateOfBirth(response.get("dateOfBirth") + "");
		fitbitUser.setDistanceUnit(response.get("distanceUnit") + "");
		fitbitUser.setFoodsLocale(response.get("foodsLocale") + "");
		fitbitUser.setFullName(response.get("fullName") + "");
		fitbitUser.setGlucoseUnit(response.get("glucoseUnit") + "");
		fitbitUser.setLocale(response.get("locale") + "");
		fitbitUser.setFullName(response.get("fullName") + "");
		fitbitUser.setMemberSince(response.get("memberSince") + "");
		fitbitUser.setOffsetFromUTCMillis(response.get("offsetFromUTCMillis") + "");
		fitbitUser.setStartDayOfWeek(response.get("startDayOfWeek") + "");
		fitbitUser.setStrideLengthRunning(response.get("strideLengthRunning") + "");
		fitbitUser.setStrideLengthRunningType(response.get("strideLengthRunningType") + "");
		fitbitUser.setStrideLengthWalking(response.get("strideLengthWalking") + "");
		fitbitUser.setStrideLengthWalkingType(response.get("strideLengthWalkingType") + "");
		fitbitUser.setTimezone(response.get("timezone") + "");
		fitbitUser.setWaterUnit(response.get("waterUnit") + "");
		fitbitUser.setWaterUnitName(response.get("waterUnitName") + "");
		fitbitUser.setWeight((double) response.get("weight"));
		fitbitUser.setWeightUnit(response.get("weightUnit") + "");
		fitbitUser.setAccessToken(ACCESS_TOKEN);
		
		//TODO: TO SAVE FitbitUser
		fitbitUserService.save(fitbitUser);
		//return new ResponseEntity<FitbitAuthentication>(fitbitAuthentication, HttpStatus.OK);
		return "redirect:/dev";
	}
	
	@RequestMapping(value="/user-profile/{id}", method=RequestMethod.GET)
	public ResponseEntity<FitbitUser> userProfile(@PathVariable("id") String id){

		FitbitUser fitbitUser = fitbitUserService.findOne(id);
		
		return new ResponseEntity<FitbitUser>(fitbitUser, HttpStatus.OK);
	}
	
	//GET https://api.fitbit.com/1/user/[user-id]/activities/heart/date/[date]/[period].json
	@SuppressWarnings("unchecked")
	@ApiOperation(value="Get Heart Rate Time Series")
	@RequestMapping(value="/user/{userId}/activities/heart/date/{date}/{period}", method=RequestMethod.GET)
	public ResponseEntity<Map<String, Object>> heartActivities(@ApiParam(value="userId logged in", required=true, defaultValue="4WTKFP") @PathVariable(value="userId") String userId, 
															   @ApiParam(value="The date, in the format yyyy-MM-dd or today.", required=true, defaultValue="today") @PathVariable(value="date") String date,
															   @ApiParam(value="The range for which data will be returned. Options are 1d, 7d, 30d, 1w, 1m.", required=true, defaultValue="1d") @PathVariable(value="period") String period ){
		RestTemplate restTemplate = new RestTemplate();
		
		HttpHeaders headers = new HttpHeaders();
		headers.add("Authorization", "Bearer "+ACCESS_TOKEN);
		
		HttpEntity<Object> requestBody = new HttpEntity<>(headers);
		Map<String, Object> response = (Map<String, Object>) restTemplate.exchange("https://api.fitbit.com/1/user/{userId}/activities/heart/date/{date}/{period}.json", HttpMethod.GET, requestBody, Map.class, userId, date, period).getBody();
		
		return new ResponseEntity<Map<String, Object>>(response, HttpStatus.OK);
	}
	//GET https://api.fitbit.com/1/user/-/activities/heart/date/[date]/[end-date]/[detail-level].json
	@SuppressWarnings("unchecked")
	@ApiOperation(value="Get Heart Rate Intraday Time Series")
	@RequestMapping(value="/user/{userId}/activities/heart/date/{date}/{end-date}/{detail-level}", method=RequestMethod.GET)
	public ResponseEntity<Map<String, Object>> heartActivitiesDateAndDetailsLevel(@ApiParam(value="userId logged in", required=true, defaultValue="4WTKFP") @PathVariable(value="userId") String userId, 
																				  @ApiParam(value="The date, in the format yyyy-MM-dd or today.", required=true, defaultValue="today") @PathVariable(value="date") String date,
																				  @ApiParam(value="The end date of the range.", required=true, defaultValue="1d") @PathVariable(value="end-date") String endDate,
																				  @ApiParam(value="Number of data points to include. Either 1sec or 1min. Optional.", required=false, defaultValue="1sec") @PathVariable(value="detail-level") String detailsLevel ){
		RestTemplate restTemplate = new RestTemplate();
		
		HttpHeaders headers = new HttpHeaders();
		headers.add("Authorization", "Bearer "+ACCESS_TOKEN);
		
		HttpEntity<Object> requestBody = new HttpEntity<>(headers);
		Map<String, Object> response = (Map<String, Object>) restTemplate.exchange("https://api.fitbit.com/1/user/{userId}/activities/heart/date/{date}/{end-date}/{detail-level}.json", HttpMethod.GET, requestBody, Map.class, userId, date, endDate, detailsLevel).getBody();
		
		return new ResponseEntity<Map<String, Object>>(response, HttpStatus.OK);
	}
	//GET https://api.fitbit.com/1/user/-/activities/heart/date/[date]/[end-date]/[detail-level]/time/[start-time]/[end-time].json
	@SuppressWarnings("unchecked")
	@ApiOperation(value="Get Heart Rate Intraday Time Series")
	@RequestMapping(value="/user/{userId}/activities/heart/date/{date}/{end-date}/{detail-level}/time/{start-time}/{end-time}", method=RequestMethod.GET)
	public ResponseEntity<Map<String, Object>> heartActivitiesWithTime(@ApiParam(value="userId logged in", required=true, defaultValue="4WTKFP") @PathVariable(value="userId") String userId, 
																				  @ApiParam(value="The date, in the format yyyy-MM-dd or today.", required=true, defaultValue="today") @PathVariable(value="date") String date,
																				  @ApiParam(value="The end date of the range.", required=true, defaultValue="1d") @PathVariable(value="end-date") String endDate,
																				  @ApiParam(value="Number of data points to include. Either 1sec or 1min. Optional.", required=false, defaultValue="1sec") @PathVariable(value="detail-level") String detailsLevel,
																				  @ApiParam(value="The start of the period, in the format HH:mm. Optional.", required=false, defaultValue="00:00") @PathVariable(value="start-time") String startTime,
																				  @ApiParam(value="The end of the period, in the format HH:mm. Optional", required=false, defaultValue="23:59") @PathVariable(value="end-time") String endTime ){
		RestTemplate restTemplate = new RestTemplate();
		
		HttpHeaders headers = new HttpHeaders();
		headers.add("Authorization", "Bearer "+ACCESS_TOKEN);
		
		HttpEntity<Object> requestBody = new HttpEntity<>(headers);
		Map<String, Object> response = (Map<String, Object>) restTemplate.exchange("https://api.fitbit.com/1/user/{userId}/activities/heart/date/{date}/{end-date}/{detail-level}/time/{start-time}/{end-time}.json", HttpMethod.GET, requestBody, Map.class, userId, date, endDate, detailsLevel, startTime, endTime).getBody();
		
		return new ResponseEntity<Map<String, Object>>(response, HttpStatus.OK);
	}
	
}
